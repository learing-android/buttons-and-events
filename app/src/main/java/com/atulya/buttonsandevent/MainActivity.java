package com.atulya.buttonsandevent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void tapToDisable(View view){

        view.setEnabled(false);
    }

    public void  youAreGreat(View view){

        Button button = (Button) view;
        button.setText("Great!!");
    }

    public void tapMe(View view){

        TextView textView = findViewById(R.id.textView);

        textView.setText("and beautiful!");
    }

    public void dontTapMe(View view){
        TextView hello = findViewById(R.id.hello);
        hello.setText("\uD83D\uDE20");

        findViewById(R.id.button).setEnabled(false);
        findViewById(R.id.button2).setEnabled(false);
        findViewById(R.id.button3).setEnabled(false);

        findViewById(R.id.button4).setEnabled(false);
    }



}